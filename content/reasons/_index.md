---
title: "Reasons for a Resident Dividend"
slug: "reasons"
image: pic02.jpg
date: 2018-03-12T22:27:21-05:00
draft: false
---

How a resident dividend promotes justice, self-determination, and prosperity.
