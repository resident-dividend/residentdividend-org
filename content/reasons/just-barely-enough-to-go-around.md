---
title: "There's just barely enough to go around"
description: "Equality of opportunity means making wealth more equal."
slug: "just-barely-enough-to-go-around"
image: pic08.jpg
keywords: ""
categories: 
    - ""
    - ""
date: 2017-10-31T22:26:13-05:00
draft: false
---

The first thing you realize when you start looking seriously into wealth redistribution is that *there's not that much wealth*.

If there were 100% redistribution of the $50 trillion of land and building wealth in the United States, each of the United States approximately 350 million people would have $142,857.

That's pretty good, and more total wealth than most of us have, but it's really not much when we're talking about wealth.  And one-shot total , and won't do anything about inequality in a decade's time

The 50th year jubilee process of redistributing 

Instead, we can have a rolling jubilee.


$2,857

If we decide we want to make up for some lost time, a five percent annual dividend would be $7,143 for each person.

Because the total amount of wealth is relatively small 

Gross inequality makes it seem like there is more wealth than there is, because so much of it is piled in a few places (even as we can't see this paper wealth, and it's hard to grasp just how obscene the inequality is, it's still concentrated enough to seem like more than it would if it were spread more fairly).  Gross inequality is also *why* there isn't more wealth than there is, because wealthy people use wealth less efficiently (parked in stocks or empty luxury apartments or in 300 foot yachts versus paying for college or bootstrapping a business or fixing up a neighborhood hangout).


Ideally, we would have been redistributing two percent of all wealth annually since dropping the full reset every 50 years required in the Biblical Jubilee.

We would have far, far, far more wealth now if—for centuries or decades or even years—everyone had been able to invest in themselves, their families, and the people they know.


### Citations

$360 trillion in global wealth — from a [2019 Credit Suisse report](https://www.credit-suisse.com/about-us/en/reports-research/global-wealth-report.html ) analyzing the household wealth of 5.1 billion people— 



$50 trillion of land and building wealth

Looking for "land wealth of the united states" and ""total value of US real estate" 

"The total size of commercial real estate in the U.S. was estimated $16 trillion in 2018." - [Nareit](https://www.reit.com/data-research/research/nareit-research/estimating-size-commercial-real-estate-market-us )
"U.S. housing market value climbs to $33.3 trillion in 2018" - [Housingwire](https://www.housingwire.com/articles/47847-us-housing-market-value-climbs-to-333-trillion-in-2018/ )


2013 calculation based on Federal Reserve's Flow of Funds report: 



#### Unused citations

extreme racial inequality in ownership of rural land: https://inequality.org/research/owns-land/