---
title: "Justice by any name"
description: "Universal rebate for city residents"
slug: "justice-by-any-name-universal-rebate-for-city-residents"
# image: pic09.jpg
keywords: ""
categories:
    - "meta"
date: 2020-03-07T23:53:09-05:00
draft: false
---

## Any concept needs a name

Resident dividends are a particular tactic for economic justice.

In effect, they would have an effect like a universal basic income, but would be driven by a recognition that when control over our own lives depends on control of capital, having people not have any, it's bad for most of us, and it's bad for society as a whole.


Andrew Yang, running a brief but remarkable nearly single-issue campaign for the Democratic nomination for president, settled on the term Freedom Dividend.  The proposed $1,000 a month would hardly have been enough to confer freedom

Citizens' dividend may be the nicest sounding, but risks being taken in the literal legal sense, and excluding non-citizens who live and work and make places communities would be a massive miscariage of justice.

Burgher Dividend
Townie Rebate
Townie Dividend

Resident Rebate



