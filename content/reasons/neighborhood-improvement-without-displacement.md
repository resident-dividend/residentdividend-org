---
title: "Neighborhood Improvement Without Displacement"
description: ""
slug: "neighborhood-improvement-without-displacement"
image: pic10.jpg
keywords: ""
categories:
    - "gentrification"
    - "right to remain"
    - "right to the city"
date: 2017-10-31T21:28:43-05:00
draft: false
---

People are pushed out

A resident dividend gives people who live in a place more resources to make their home there.

when funded through wealth taxes (to the extent wealth taxes are recoverable by a local government), the ability to remain is further supported by penalizing speculators, corporate landlords, and



***

the distribution of economic rewards is unfair, and this isn't just an academic or sporting concern.  Life outcomes are massively affected by this outcome: How long we live, how much control we have over our lives in the meantime, and our chances for making an impact on the world

redistribution of wealth to the extent possible at the local level.

***

With the [land value of the United States calculated at $23 trillion](http://blogs.wsj.com/economics/2015/04/22/how-much-is-the-u-s-worth-economist-values-the-land-alone-at-23-trillion/)

if each person had an equal share it would be about $75,000 per person.

Redistributing just that portion at five percent a year is $3,750.

(Precise values would be $76,667 and $3,833)

A resident dividend has all the benefits of (and of course effectively is) a universal guaranteed basic income.  (See http://www.usbig.net/whatisbig.php)  The resident dividend's emphasis on being funded through redistribution of wealth

Supplementary ideas:

carbon tax
pre-funded disposal costs for all physical objects


How property tax is actually calculated:
http://www.house.leg.state.mn.us/hrd/pubs/ss/ssptterm.pdf


property taxes of cities versus suburbs
https://streets.mn/2016/01/03/minneapolis-property-taxes-are-high-why/
