---
title: "Why & How"
description: "In brief"
slug: "why-and-how-in-brief"
# image: pic09.jpg
keywords: ""
categories:
    - "overview"
    - "how"
    - "why"
date: 2018-03-18T15:26:09-05:00
draft: false
---

## Why

People lack the resources to invest in themselves, in their own communities, in their own success, and—terrifyingly, as reflected in wealth-related life expectancy gaps measured in decades—in their own survival.

This lack of resources is reflected in and compounded by:

* Disinvestment, [historical](https://dsl.richmond.edu/panorama/redlining/#loc=13/44.9933/-93.3024&opacity=0.8&city=minneapolis-mn) and present-day.
* Lack of political power.
* Gentrification-driven displacement.
* Outsize economic rewards to parasitic speculators.

## How

If we're going to continue to have capitalism, everybody needs capital.  If we're going to have a market economy, everyone needs money.

## Citations

life expectancy gaps measured in decades: 
