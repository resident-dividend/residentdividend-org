# Seeking an analysis of a local redistribution of wealth

A universal, equal resident dividend funded by a tax on all practically identifiable local wealth—land, buildings, anything that is relatively nailed down—is a potential method to mitigate some of the worst aspects of capitalism in places with high economic inequality.

<!-- wp:paragraph -->
<p>I will admit this is not the most exciting political economy.  It would help a large majority of people anywhere it were carried out, protecting people from malnutrition, losing housing, the lower-paying of exploitative employment relationships, abusive interpersonal relationships they are bound to by constrained resources, and being pushed out of an area entirely by gentrification.  On the positive and more speculative side, greater equality of wealth may spur more cooperative ventures— worker, consumer, investor, and even housing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>What i am hoping someone can help with is pointing to or running an analysis of the more prosaic expected effects.  Renters (and unhoused people!) would have relatively more money, but how much would be expected to be clawed back by property owners?  How much might property values fall, with the expected taxation burden of the most valuable properties going up?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In the US context, local property taxes frequently run about 1–2% of property values for people living in their homes— enough to be a serious burden for people who become income-restrained, even if their house is modest by local standards.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>It probably would not matter to the analysis, but presume each person of every age receives an equal amount, with families receiving half the allotment for children under 16, with the other half invested in municipal bonds with this savings account being turned over with access to withdrawing a portion when reaching 16, and progressively more over the next five years, with the balance made available at age 21.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>For a consideration that may matter to a dynamic analysis, let us presume everyone presently living in the area is automatically included, but newcomers are treated as gaining full residency progressively over the course of five years.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>To provide some concrete numbers, in Hennepin County, <a href="https://www.hennepin.us/-/media/hennepinus/residents/property/documents/property-assessment-report-2021.pdf">the total estimated market value of real property is currently about $205 billion</a>.  The <a href="https://en.wikipedia.org/wiki/Hennepin_County,_Minnesota#Demographics">population</a> is around 1,300,000 people.  Let us say a full 2%, $4.1 billion, from property values will go to government services.  Let us further tax another 10% of property tax values, $20.5 billion, for the resident dividend.  Each each person receives $15,769 per year ($1,314 per month) from the resident dividend.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Despite the total 12% property tax rate, the financial situation of the mean property taxpayer is unchanged.  And the mean taxpayer is already a disproportionately wealthy person or entity— the median individual is receiving a net $12,000 per year, or $1,000 per month, more than they would have otherwise.</p>
<!-- /wp:paragraph -->

That, at least, is the static analysis.  What do more sophisticated dynamic analyses predict?


Version posted: https://casperforum.org/blog/seeking-an-analysis-of-a-local-redistribution-of-wealth/
